package lesson10;
import java.io.IOException;
class bake
{
    boolean turn = false;
    int temperature;

    void turtOnOff (boolean state)
    {
        turn = state;
    }
    void cook (int foodTemp)
    {
        foodTemp = temperature;
    }
}
class gasBake extends bake
{
    int gas = 0;

    void changeTemperature (int desiredTemperature)
    {
        desiredTemperature = gas ;
    }
}
class elBake extends bake
{
    int mainsVoltage = 0;

    void changeTemperature (int desiredTemperature)
    {
        desiredTemperature = mainsVoltage ;
    }
}
public class lesson10 {

    public static void main(String[] args) throws IOException {
        gasBake gasBake = new gasBake();
        elBake elBake = new elBake();
        gasBake.turtOnOff(true);
        gasBake.cook(200);
        gasBake.changeTemperature(200);
        gasBake.turtOnOff(false);
        elBake.turtOnOff(true);
        elBake.cook(100);
        elBake.changeTemperature(100);
        elBake.turtOnOff(false);
    }
}