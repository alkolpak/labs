package lesson13;

public class lesson13 {
    static class PigCipher {

        char[] vowels = {'a', 'e', 'i', 'o', 'u'};

        public boolean isVowel(char vowel) {
            for (char v : vowels) {
                if (vowel == v) {
                    return true;
                }
            }
            return false;
        }

        String encrypt(String source) {
            source = source.toLowerCase();
            char firstChar = source.charAt(0);
            if (this.isVowel(firstChar)) {
                return source + "ay";
            }
            String temp = "";
            for (int i = 0; i < source.length(); i += 1) {
                if (!this.isVowel(source.charAt(i))) {
                    temp += source.charAt(i);
                } else {
                    break;
                }
            }
            return source.substring(temp.length()) + temp + "ay";
        }

    }
    static class Cipher extends PigCipher
    {
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        String source;

        public void setSource(){
            this.source = source;
        }

        public String getSource() {
            return source;
        }
        String word(String source) {
            String out = "";
            String[] words = source.split("\\s");
            for(int j = 0; j < words.length; j++)
            {
                out +=encrypt(words[j]) + " ";
            }
            return out;
        }

    }
    public static void main(String[] args) {
        PigCipher cipher = new PigCipher();
        System.out.println(cipher.encrypt("poper"));
        Cipher cipher1 = new Cipher();
        System.out.println(cipher1.word("popper catter coffe"));
    }
}
