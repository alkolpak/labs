package lesson13;

public class lesson13_1 {
    static class CaesarCipher {
        String encrypt(String source, int offset) {
            String result = "";
            for (int i = 0; i < source.length(); i += 1) {
                char currentChar = source.charAt(i);
                int currentCharPosition = currentChar - 'A';
                int newCharPosition = (currentCharPosition + offset) % 26;
                char newChar = (char) ('A' + newCharPosition);
                result += newChar;
            }
            return result;
        }
        String decrypt(String source, int offset)
        {
            String result = "";
            for (int i = 0; i < source.length(); i += 1) {
                char currentChar = source.charAt(i);
                if(currentChar >= 'A' && currentChar <= 'F')
                    currentChar+= 26;
                int currentCharPosition = currentChar - 'A';
                int oldCharPosition = (currentCharPosition - offset) + 26;
                char newChar = (char) ('A' + oldCharPosition);
                result += newChar;
            }
            return result;
        }
    }
    static class Cipher extends CaesarCipher
    {
        String source;

        public void setSource(){
            this.source = source;
        }

        public String getSource() {
            return source;
        }
        String word(String source) {
            String out = "";
            String[] words = source.split("\\s");
            for(int j = 0; j < words.length; j++)
            {
                out +=encrypt(words[j], j ) + " ";
            }
            return out;
        }
        String decrypt(String source)
        {
            String out = "";
            String[] words = source.split("\\s");
            for(int j = 0; j < words.length; j++)
            {
                out +=decrypt(words[j], j ) + " ";
            }
            return out;
        }
    }
    public static void main(String[] args) {
        CaesarCipher cipher = new CaesarCipher();
        System.out.println(cipher.encrypt("popper", 0));
        System.out.println(cipher.decrypt("VUVVKX", 0));
        Cipher cipher1 = new Cipher();
        System.out.println(cipher1.word("popper catter coffe print "));
        System.out.println(cipher1.decrypt("VUVVKX JHAALY KWNNM YARWC "));
    }
}

