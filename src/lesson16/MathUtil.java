package lesson16;

public class MathUtil {
    public static float add(float value1, float value2) {
        return value1 + value2;
    }

    public static float subtract(float value1, float value2) {
        return value1 - value2;
    }

    public static float multiply(float value1, float value2) {
        return value1 * value2;
    }

    public static float divide(float value1, float value2) {
        return value1 / value2;
    }
}
