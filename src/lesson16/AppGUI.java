package lesson16;

import javax.swing.*;

public class AppGUI extends JFrame {
    public AppGUI() {
        setBounds(200, 200, 260, 450);
        setTitle("Калькулятор");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new CalculatorPanel());
        setVisible(true);
    }
}