package lesson7;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class lesson7 {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        int userChoice = scanner.nextInt();
        while(userChoice != 9) {
            int opponentValue = (int) Math.round(Math.random() * 4);

            ArrayList<String> name = new ArrayList<>();
            name.add(0, "Камень");
            name.add(1, "Ножницы");
            name.add(2, "Бумага");
            name.add(3, "Ящерица");
            name.add(4, "Спок");

            String userCh = name.get(userChoice), opponentCh = name.get(opponentValue);
            System.out.println("Вы выбрали " + userCh + ", ваш оппонет выбрал " + opponentCh);
            if(userChoice == 0 && opponentValue == 0)
                System.out.println("Ничья!");
            if(userChoice == 0 && opponentValue == 1)
                System.out.println("Вы победили");
            if(userChoice == 0 && opponentValue == 2)
                System.out.println("Вы проиграли");
            if(userChoice == 0 && opponentValue == 3)
                System.out.println("Вы победили");
            if(userChoice == 0 && opponentValue == 4)
                System.out.println("Вы проиграли");
            if(userChoice == 1 && opponentValue == 0)
                System.out.println("Вы проиграли");
            if(userChoice == 1 && opponentValue == 1)
                System.out.println("Ничья!");
            if(userChoice == 1 && opponentValue == 2)
                System.out.println("Вы победили");
            if(userChoice == 1 && opponentValue == 3)
                System.out.println("Вы победили");
            if(userChoice == 1 && opponentValue == 4)
                System.out.println("Вы проиграли");
            if(userChoice == 2 && opponentValue == 0)
                System.out.println("Вы победили");
            if(userChoice == 2 && opponentValue == 1)
                System.out.println("Вы проиграли");
            if(userChoice == 2 && opponentValue == 2)
                System.out.println("Ничья!");
            if(userChoice == 2 && opponentValue == 3)
                System.out.println("Вы проиграли");
            if(userChoice == 2 && opponentValue == 4)
                System.out.println("Вы победили");
            if(userChoice == 3 && opponentValue == 0)
                System.out.println("Вы проиграли");
            if(userChoice == 3 && opponentValue == 1)
                System.out.println("Вы проиграли");
            if(userChoice == 3 && opponentValue == 2)
                System.out.println("Вы победили");
            if(userChoice == 3 && opponentValue == 3)
                System.out.println("Ничья!");
            if(userChoice == 3 && opponentValue == 4)
                System.out.println("Вы победили");
            if(userChoice == 4 && opponentValue == 0)
                System.out.println("Вы победили");
            if(userChoice == 4 && opponentValue == 1)
                System.out.println("Вы победили");
            if(userChoice == 4 && opponentValue == 2)
                System.out.println("Вы проиграли");
            if(userChoice == 4 && opponentValue == 3)
                System.out.println("Вы проиграли");
            if(userChoice == 4 && opponentValue == 4)
                System.out.println("Ничья!");


            userChoice = scanner.nextInt();
        }
    }
}
