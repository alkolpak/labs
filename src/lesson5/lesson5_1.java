package lesson5;

import java.util.Scanner;
    //задание 5
public class lesson5_1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input num: ");
        int num = in.nextInt();

        while (num != 1 && num % 2 == 0) {
            num /= 2;
        }

        System.out.println(num == 1 ? "YES" : "NO");
    }
}
