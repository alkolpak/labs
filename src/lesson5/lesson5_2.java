package lesson5;
public class lesson5_2 {
    //задание 1
    public static void main(String[] args) throws Exception {
        int x = -1;
        System.out.println("Введите цифру от 0 до 3");
        x = System.in.read();
        switch (x) {
            case (0):
                System.out.println("Зима");
                break;
            case (1):
                System.out.println("Весна");
                break;
            case (2):
                System.out.println("Осень");
                break;
            case (3):
                System.out.println("Лето");
                break;
            default:
                System.out.println("Не введено требуемых значений");
                break;
        }
    }
}