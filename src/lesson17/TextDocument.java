package lesson17;

import java.util.Date;

public class TextDocument extends Document {
    private String data = "";

    public TextDocument() {
    }

    public TextDocument(Date creationDate, int size) {
        super(creationDate, size);
    }

    @Override
    public String read() {
        System.out.println("Чтение из текстового файла");
        return data;
    }

    @Override
    public void write(Object data) {
        String text;
        try {
            text = (String) data;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException();
        }
        write(text);
    }

    public void write(String data) {
        System.out.println("Запись в текстовый файл");
        this.data = data;
    }

    public void print() {
        System.out.println("Печать текстового файла");
    }

}
