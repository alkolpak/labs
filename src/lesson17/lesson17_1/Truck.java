package lesson17.lesson17_1;

public class Truck extends Car {
    private int payloadCapacity;
    private int payload;

    public Truck(int weight, int enginePower, int payloadCapacity) {
        super(weight, enginePower);
        this.payloadCapacity = payloadCapacity;
        this.payload = 0;
    }

    public int getPayloadCapacity() {
        return payloadCapacity;
    }

    public int getPayload() {
        return payload;
    }

    public void load(int amount) {
        if (payload + amount > payloadCapacity) {
            System.out.println("Превышена грузовая вместимость");
        }
        else {
            System.out.printf("Загружаем груз объёмом %d", amount);
            payload += amount;
        }
    }

    public void unload(int amount) {
        if (amount > payload) {
            System.out.println("В кузове нет такого количества груза");
        }
        else {
            System.out.printf("Выгружаем груз объёмом %d", amount);
            payload -= amount;
        }
    }
}
