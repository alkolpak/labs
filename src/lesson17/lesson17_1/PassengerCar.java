package lesson17.lesson17_1;

public class PassengerCar extends Car {
    private int passengerCapacity;
    private int passengerCount;

    public PassengerCar(int weight, int enginePower, int passengerCapacity) {
        super(weight, enginePower);
        this.passengerCapacity = passengerCapacity;
        this.passengerCount = 0;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public int getPassengerCount() {
        return passengerCount;
    }

    public void pickPassengers(int num) {
        if (passengerCount + num > passengerCapacity) {
            System.out.println("Превышена пассажирская вместимость");
        }
        else {
            System.out.printf("Сажаем %d пассажиров", num);
            passengerCount += num;
        }
    }

    public void dropPassengers(int num) {
        if (num > passengerCount) {
            System.out.println("В машине нет указанного количества пассажиров");
        }
        else {
            System.out.printf("Высаживаем %d пассажиров", num);
            passengerCount -= num;
        }
    }
}
