package lesson17.lesson17_1;

public abstract class Car {
    private int weight;
    private int enginePower;

    public Car(int weight, int enginePower) {
        this.weight = weight;
        this.enginePower = enginePower;
    }

    public int getWeight() {
        return weight;
    }

    public int getEnginePower() {
        return enginePower;
    }
}
