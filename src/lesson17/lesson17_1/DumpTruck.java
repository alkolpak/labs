package lesson17.lesson17_1;

public class DumpTruck extends Truck {
    boolean bodyLifted;

    public DumpTruck(int weight, int enginePower, int payloadCapacity) {
        super(weight, enginePower, payloadCapacity);
        bodyLifted = false;
    }

    public boolean isBodyLifted() {
        return bodyLifted;
    }

    public void liftBody() {
        if (bodyLifted) {
            System.out.println("Кузов уже поднят");
        }
        else {
            System.out.println("Поднимаем кузов");
        }
    }

    public void lowerBody() {
        if (!bodyLifted) {
            System.out.println("Кузов уже опущен");
        }
        else {
            System.out.println("Опускаем кузов");
        }
    }
}
