package lesson17;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BinaryDocument extends Document {
    private List<Byte> data = new ArrayList<>();

    public BinaryDocument() {
    }

    public BinaryDocument(Date creationDate, int size) {
        super(creationDate, size);
    }

    @Override
    public List<Byte> read() {
        System.out.println("Чтение из двоичного файла");
        return data;
    }

    @Override
    public void write(Object data) {
        List<Byte> bytes;
        try {
            bytes = (List<Byte>) data;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException();
        }
        write(bytes);
    }

    public void write(List<Byte> data) {
        System.out.println("Запись в двоичный файл");
        this.data = data;
    }

    public void run () {
        System.out.println("Двоичный файл выполняется");
    }
}
