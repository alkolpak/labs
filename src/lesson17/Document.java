package lesson17;

import java.util.Date;

public abstract class Document {
    protected Date creationDate;
    protected int size;

    public Document() {
        creationDate = new Date();
        size = 0;
    }

    public Document(Date creationDate, int size) {
        if (size < 0 || creationDate == null) throw new IllegalArgumentException();
        this.creationDate = creationDate;
        this.size = size;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public int getSize() {
        return size;
    }

    public abstract Object read();

    public abstract void write(Object data);

}
