package lesson18;

import java.util.Date;

public class TimestampedReport extends Report {
    public TimestampedReport(String reportText) {
        super(reportText);
    }

    @Override
    public String getReport() {
        return String.format("%d: %s: %s", getReportNumber(), new Date(System.currentTimeMillis()).toString(), getReportText());
    }
}
