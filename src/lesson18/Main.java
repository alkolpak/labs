package lesson18;

/**
 *   Класс для тестирования
 */
public class Main {
    public static void main(String[] args) {
        Report report1 = new Report("Cool stuff");
        Report report2 = new TimestampedReport("Cool stuff2");
        Report report3 = new UsernameReport("Cool stuff3");

        Reporter reporter = new Reporter();

        reporter.report(report1);
        reporter.report(report2);
        reporter.report(report3);
    }
}
