package lesson18;

public class Report {
    private static int reportCount = 0;
    private String reportText;
    private int reportNumber;

    public Report(String reportText) {
        this.reportText = reportText;
        reportCount++;
        this.reportNumber = reportCount;
    }

    public String getReport() {
        return String.format("%d: %s", getReportNumber(), getReportText());
    }

    protected int getReportNumber() {
        return reportNumber;
    }

    protected String getReportText() {
        return reportText;
    }
}
