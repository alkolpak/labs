package lesson18;

public class UsernameReport extends Report {
    public UsernameReport(String reportText) {
        super(reportText);
    }

    @Override
    public String getReport() {
        return String.format("%d: %s: %s", getReportNumber(), System.getProperty("user.name"), getReportText());
    }
}
