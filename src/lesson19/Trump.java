package lesson19;

public class Trump implements Playable {
    private final int diameter;
    final String KEY = "Си бемоль";

    public Trump() {
        this(110);
    }

    public Trump(int diameter) {
        this.diameter = diameter;
    }

    @Override
    public void play() {
        System.out.printf("Играет труба диаметром %d и с тональностью \"%s\" %n", diameter, KEY);
    }
}
