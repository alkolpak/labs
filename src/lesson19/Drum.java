package lesson19;

public class Drum implements Playable {
    private final int size;
    final String KEY = "До минор";

    public Drum() {
        this(250);
    }

    public Drum(int size) {
        this.size = size;
    }

    @Override
    public void play() {
        System.out.printf("Играет барабан размером %d и с тональностью \"%s\" %n", size, KEY);
    }
}
