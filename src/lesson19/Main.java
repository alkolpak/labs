package lesson19;

public class Main {
    public static void main(String[] args) {
        Playable[] instruments = new Playable[] {
                new Guitar(),
                new Drum(),
                new Bass(),
                new Trump()
        };

        for (Playable p : instruments) {
            p.play();
        }
    }
}
