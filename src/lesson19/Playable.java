package lesson19;

public interface Playable {
    final String KEY = "До мажор";

    default void play() {
        System.out.printf("Играет инструмент с тональностью \"%s\" %n", KEY);
    }
}
