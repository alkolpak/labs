package lesson19;

public class Guitar implements Playable {
    private final int stringsNumber;
    final String KEY = "Ми минор";

    public Guitar() {
        this(7);
    }

    public Guitar(int stringsNumber) {
        this.stringsNumber = stringsNumber;
    }

    @Override
    public void play() {
        System.out.printf("Играет гитара с количеством струн %d и с тональностью \"%s\" %n", stringsNumber, KEY);
    }
}
