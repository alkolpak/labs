package lesson21_22;

import javax.swing.*;
import java.awt.*;

public class GameFrame extends JFrame {

   public GameFrame() {
       this.setTitle("Арканоид");
       this.setSize(new Dimension(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT));
       this.setResizable(false);
       this.setFocusable(true);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setLocationRelativeTo(null);
       GamePanel gamePanel = new GamePanel();
       this.add(gamePanel);
       this.setVisible(true);
       gamePanel.grabFocus();
       this.pack();
       gamePanel.requestFocusInWindow();
   }

}
