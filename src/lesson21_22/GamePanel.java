package lesson21_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GamePanel extends JPanel implements ActionListener, KeyListener {
    private final String HIGHSCORE_FILENAME = "highscore.txt";
    private final Brick[] bricks;
    private final Ball ball;
    private final Paddle paddle;
    private final Timer timer;

    private int score = 0;
    private int highScore;

    public GamePanel() {

        try {
            FileReader reader = new FileReader(HIGHSCORE_FILENAME);
            highScore = reader.read();
            if (highScore < 0) highScore = 0;
        } catch (Exception e) {
            highScore = 0;
        }

        this.setFocusable(true);
        this.setPreferredSize(new Dimension(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT));
        this.addKeyListener(this);
        int brickCounter = 0;
        bricks = new Brick[Constants.BRICK_COUNT];
        for (int i = 0; i < 5; i += 1) {
            for (int j = 0; j < 6; j += 1) {
                bricks[brickCounter] = new Brick(j * 40 + 30, i * 10 + 50);
                brickCounter += 1;
            }
        }
        ball = new Ball();
        paddle = new Paddle();
        timer = new Timer(Constants.TIMER_PERIOD, this);
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        this.drawObjects(g2d);
    }

    void drawObjects(Graphics2D g2d) {
        for (int i = 0; i < Constants.BRICK_COUNT; i += 1) {
            if (!bricks[i].isDestroyed()) {
                g2d.drawImage(bricks[i].getImage(), bricks[i].getX(), bricks[i].getY(), bricks[i].getImageWidth(), bricks[i].getImageHeight(), this);
            }
        }
        g2d.drawImage(ball.getImage(), ball.getX(), ball.getY(), ball.getImageWidth(), ball.getImageHeight(), this);
        g2d.drawImage(paddle.getImage(), paddle.getX(), paddle.getY(), paddle.getImageWidth(), paddle.getImageHeight(), this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.ball.move();
        this.paddle.move();
        this.detectCollisions();
        this.repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        paddle.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        paddle.keyReleased(e);
    }

    private void detectCollisions() {
        if (ball.getRect().getMaxY() > Constants.SCREEN_BOTTOM) {
            System.out.println("Game Over");
            endGame();
        }
        if (ball.getRect().intersects(paddle.getRect())) {
            int paddleXPos = (int) paddle.getRect().getMinX();
            int ballXPos = (int) ball.getRect().getMinX();
            int firstPoint = paddleXPos + 8;
            int secondPoint = paddleXPos + 16;
            int thirdPoint = paddleXPos + 24;
            int fourthPoint = paddleXPos + 32;
            if (ballXPos < firstPoint) {
                ball.setDirectionX(-1);
                ball.setDirectionY(-1);
            }
            if (ballXPos >= firstPoint && ballXPos < secondPoint) {
                ball.setDirectionX(-1);
                ball.setDirectionY(-1 * ball.getDirectionY());
            }
            if (ballXPos >= secondPoint && ballXPos < thirdPoint) {
                ball.setDirectionX(0);
                ball.setDirectionY(-1);
            }
            if (ballXPos >= thirdPoint && ballXPos < fourthPoint) {
                ball.setDirectionX(1);
                ball.setDirectionY(-1 * ball.getDirectionY());
            }
            if (ballXPos >= fourthPoint) {
                ball.setDirectionX(1);
                ball.setDirectionY(-1);
            }
        }
        boolean winCondition = true;
        for (int i = 0; i < Constants.BRICK_COUNT; i += 1) {
            if (!bricks[i].isDestroyed()) winCondition = false;
            if (ball.getRect().intersects(bricks[i].getRect())) {
                if (!bricks[i].isDestroyed()) {
                    int ballLeft = (int) ball.getRect().getMinX();
                    int ballHeight = (int) ball.getRect().getHeight();
                    int ballWidth = (int) ball.getRect().getWidth();
                    int ballTop = (int) ball.getRect().getMinY();
                    Point pointRight = new Point(ballLeft + ballWidth + 1, ballTop);
                    Point pointLeft = new Point(ballLeft - 1, ballTop);
                    Point pointTop = new Point(ballLeft, ballTop - 1);
                    Point pointBottom = new Point(ballLeft, ballTop + ballHeight + 1);
                    if (bricks[i].getRect().contains(pointRight)) {
                        ball.setDirectionX(-1);
                    } else if (bricks[i].getRect().contains(pointLeft)) {
                        ball.setDirectionX(1);
                    }
                    if (bricks[i].getRect().contains(pointTop)) {
                        ball.setDirectionY(1);
                    } else if (bricks[i].getRect().contains(pointBottom)) {
                        ball.setDirectionY(-1);
                    }
                    bricks[i].setDestroyed(true);
                    System.out.println("Score: " + ++score);
                }
            }
        }
        if (winCondition) {
            System.out.println("Win!");
            endGame();
        }
    }

    private void endGame() {
        timer.stop();
        if (score > highScore) {
            System.out.println("Congratulations! New high score: " + score);

            try {
                FileWriter writer = new FileWriter(HIGHSCORE_FILENAME, false);
                writer.write(score);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
