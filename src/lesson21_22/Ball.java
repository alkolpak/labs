package lesson21_22;

import javax.swing.*;

public class Ball extends Sprite {

    private int directionX;
    private int directionY;

    public Ball() {
        this.setX(Constants.BALL_START_X);
        this.setY(Constants.BALL_START_Y);
        ImageIcon imageIcon = new ImageIcon("src/resources/ball.png");
        this.setImage(imageIcon.getImage());
        this.calculateImageSize();
        this.directionX = -1;
        this.directionY = -1;
    }

    void move() {
        this.setX(this.getX() + this.directionX);
        this.setY(this.getY() + this.directionY);
        if (this.getX() == 0) {
            this.setDirectionX(1);
        }
        if (this.getY() == 0) {
            this.setDirectionY(1);
        }
        if (this.getX() >= Constants.WINDOW_WIDTH - this.getImageWidth()) {
            this.setDirectionX(-1);
        }
    }

    public int getDirectionX() {
        return directionX;
    }

    public int getDirectionY() {
        return directionY;
    }

    public void setDirectionY(int directionY) {
        this.directionY = directionY;
    }

    public void setDirectionX(int directionX) {
        this.directionX = directionX;
    }
}
