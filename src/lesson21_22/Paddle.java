package lesson21_22;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class Paddle extends Sprite {

    private int directionX;

    public Paddle() {
        ImageIcon imageIcon = new ImageIcon("src/resources/paddle.png");
        this.setImage(imageIcon.getImage());
        this.calculateImageSize();
        this.directionX = 0;
        this.setX(Constants.PADDLE_START_X);
        this.setY(Constants.PADDLE_START_Y);
    }

    void move() {
        if (this.directionX == 0) {
            return;
        }
        this.setX(this.getX() + this.directionX);
        if (this.getX() <= 0) {
            this.setX(0);
        }
        if (this.getX() >= Constants.WINDOW_WIDTH - this.getImageWidth()) {
            this.setX(Constants.WINDOW_WIDTH - this.getImageWidth());
        }
    }

    void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            this.directionX = -2;
        }
        if (key == KeyEvent.VK_RIGHT) {
            this.directionX = 2;
        }
    }

    void keyReleased(KeyEvent e) {
        this.directionX = 0;
    }

}
