package lesson21_22;

public class Constants {
    public static final int WINDOW_WIDTH = 320;
    public static final int WINDOW_HEIGHT = 400;
    public static final int BRICK_COUNT = 30;
    public static final int BALL_START_X = 230;
    public static final int BALL_START_Y = 350;
    public static final int TIMER_PERIOD = 3;
    public static final int PADDLE_START_X = 200;
    public static final int PADDLE_START_Y = 350;
    public static final int SCREEN_BOTTOM = 390;
}
