package lesson21_22;

import javax.swing.*;

public class Brick extends Sprite {

    private boolean destroyed;

    public Brick(int x, int y) {
        this.setX(x);
        this.setY(y);
        this.destroyed = false;
        ImageIcon imageIcon = new ImageIcon("src/resources/brick.png");
        this.setImage(imageIcon.getImage());
        this.calculateImageSize();
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
