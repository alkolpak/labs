package lesson14;

import static lesson14.schema.SchemaTypes.HTTP_SCHEMA;

class Url {
    private String schema = HTTP_SCHEMA;
    private String hostname;
    private String resource = "";

    public Url(String hostname) {
        this.hostname = hostname;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getSchema() {
        return this.schema;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String build() {
        String output = this.getSchema() + "://" + hostname;
        if(resource != "") {
            output = output + "/" + resource;
        }
        return output;
    }
}
