package lesson14;

public class UrlBuilder {
    private Url url;

    public UrlBuilder(String hostname) {
        url = new Url(hostname);
    }

    public UrlBuilder setSchema(String schema) {
        this.url.setSchema(schema);
        return this;
    }

    public UrlBuilder setResource(String resource) {
        this.url.setResource(resource);
        return this;
    }

    public String build() {
        return this.url.build();
    }
}