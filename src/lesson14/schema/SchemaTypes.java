package lesson14.schema;

public class SchemaTypes {
    public static final String HTTP_SCHEMA = "http";
    public static final String HTTPS_SCHEMA = "https";
    public static final String FTP_SCHEMA = "ftp";
}
