package lesson14;

import static lesson14.schema.SchemaTypes.*;

public class UrlBuilderHelper {
    public static String getHttpUrl(String hostname) {
        return new UrlBuilder(hostname)
                .setSchema(HTTP_SCHEMA)
                .build();
    }

    public static String getHttpUrl(String hostname, String resource) {
        return new UrlBuilder(hostname)
                .setSchema(HTTP_SCHEMA)
                .setResource(resource)
                .build();
    }

    public static String getHttpsUrl(String hostname) {
        return new UrlBuilder(hostname)
                .setSchema(HTTPS_SCHEMA)
                .build();
    }

    public static String getHttpsUrl(String hostname, String resource) {
        return new UrlBuilder(hostname)
                .setSchema(HTTPS_SCHEMA)
                .setResource(resource)
                .build();
    }

    public static String getFtpUrl(String hostname) {
        return new UrlBuilder(hostname)
                .setSchema(FTP_SCHEMA)
                .build();
    }

    public static String getFtpUrl(String hostname, String resource) {
        return new UrlBuilder(hostname)
                .setSchema(FTP_SCHEMA)
                .setResource(resource)
                .build();
    }
}
