package lesson3;

public class lesson3 {
    public static void main(String[] args) {
        //тип byte
        byte m, e, o, w;
        m = 2;
        e = 4;
        o = 8;
        w = (byte) (o - (m + e));
        System.out.println(w);

        //тип short
        short studentID = 10;
        System.out.println(studentID);

        //тип long
        long days = 365l;
        System.out.println(days);

        //тип int
        int number = 20000;
        System.out.println(number);

        // тип float
        float usd = 73.85f;
        System.out.println(usd);

        //тип double
        double a, b, c;
        a = 10.2;
        b = 44.68;
        c = a * b;
        System.out.println(c);

        //тип char
        char ch1;
        ch1 = 107;
        System.out.println(ch1);

        //тип boolean
        boolean x = false;
        System.out.println(x);

        //тип string
        String s1 = "cat is ";
        String s2 = "out of ";
        String s3 = "the bag";
        String proverb = s1 + s2 +s3;
        System.out.println(proverb);

    }
    }
