package lesson11.food.vegetables;

import lesson11.food.Food;

public class Cucumber implements Food {
    @Override
    public String getName() {
        return "Огурец";
    }
}
