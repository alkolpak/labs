package lesson11.food.forest;

import lesson11.food.Food;

public class Berry implements Food {
    @Override
    public String getName() {
        return "Ягода";
    }
}
