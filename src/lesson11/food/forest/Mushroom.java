package lesson11.food.forest;

import lesson11.food.Food;

public class Mushroom implements Food {
    @Override
    public String getName() {
        return "Гриб";
    }
}
