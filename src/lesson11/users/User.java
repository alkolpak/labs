package lesson11.users;

import lesson11.food.Food;

public class User {
    public void eat(Food food) {
        System.out.println("Это было " + food.getName() + ", вкусно!");
    }
}
