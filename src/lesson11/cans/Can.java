package lesson11.cans;

import lesson11.food.Food;

public class Can {
    private Food food;

    public Can(Food food) {
        this.food = food;
    }

    public Food open() {
        return food;
    }
}
