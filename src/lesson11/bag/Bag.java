package lesson11.bag;

import java.util.ArrayList;

public class Bag {
    private ArrayList<Object> content = new ArrayList<>();

    public void put(Object item) {
        content.add(item);
    }
}
