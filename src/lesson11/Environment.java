package lesson11;

import lesson11.animal.Cat;
import lesson11.animal.Dog;
import lesson11.bag.Bag;
import lesson11.cans.Can;
import lesson11.cupboards.Cupboard;
import lesson11.food.forest.Berry;
import lesson11.food.forest.Mushroom;
import lesson11.food.vegetables.Cucumber;
import lesson11.food.vegetables.Tomato;
import lesson11.robot.Terminator;
import lesson11.users.User;
import stub.Simulator;
import test.TestCase;
import test.TestChain;

public class Environment {
    public static void main(String[] args) {
        // Part 1
        System.out.println(TestCase.startTestCase());
        System.out.println(TestChain.startTestChain());
        System.out.println(Simulator.startSimulator());

        // Part 2
        Cat.sayHello();
        Dog.sayHello();
        Terminator.sayHello();

        // Part 3
        Cupboard cupboard = new Cupboard(10);
        cupboard.put(new Can(new Cucumber()));
        cupboard.put(new Can(new Tomato()));
        cupboard.put(new Can(new Mushroom()));
        cupboard.put(new Can(new Berry()));

        User user = new User();

        user.eat(cupboard.get().open());
        user.eat(cupboard.get().open());
        user.eat(cupboard.get().open());
        user.eat(cupboard.get().open());

        Bag metaBag = new Bag();
        metaBag.put(new Bag());
        metaBag.put(new Bag());
        metaBag.put(new Bag());
    }
}
