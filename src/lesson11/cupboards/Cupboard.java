package lesson11.cupboards;

import lesson11.cans.Can;

import java.util.Deque;
import java.util.LinkedList;

public class Cupboard {
    private final int maxSize;
    private int currentSize = 0;
    private Deque<Can> cans = new LinkedList<>();

    public Cupboard(int maxSize) {
        this.maxSize = maxSize;
    }

    public void put(Can can) {
        currentSize++;
        if (currentSize > maxSize) {
            System.out.println("Шкаф переполнен");
        }
        else {
            cans.push(can);
        }
    }

    public Can get() {
        currentSize--;
        if (currentSize < 0) {
            System.out.println("Шкаф пуст");
            return null;
        }
        else {
            return cans.pop();
        }
    }
}
