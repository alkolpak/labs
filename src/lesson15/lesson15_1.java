package lesson15;
import java.io.IOException;
class bake
{
    Boolean turn = false;
    Integer temperature;

    void turtOnOff (Boolean state)
    {
        turn = state;
    }
    void cook (Integer foodTemp)
    {
        if ((foodTemp > 10) && foodTemp < 150)
            if (foodTemp != temperature)
                foodTemp = temperature;
    }
}
class gasBake extends bake
{
    Integer gas = 0;

    void changeTemperature (Integer desiredTemperature)
    {
        desiredTemperature = gas ;
    }
}
class elBake extends bake
{
    Integer mainsVoltage = 0;

    void changeTemperature (Integer desiredTemperature)
    {
        desiredTemperature = mainsVoltage ;
    }
}
public class lesson15_1 {

    public static void main(String[] args) throws IOException {
        gasBake gasBake = new gasBake();
        elBake elBake = new elBake();
        gasBake.turtOnOff(true);
        gasBake.cook(200);
        gasBake.changeTemperature(200);
        gasBake.turtOnOff(false);
        elBake.turtOnOff(true);
        elBake.cook(100);
        elBake.changeTemperature(100);
        elBake.turtOnOff(false);
    }
}