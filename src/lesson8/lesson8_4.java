package lesson8;
import java.io.IOException;
import java.util.Scanner;

public class lesson8_4 {
    public static int count(String str, String target) {
        return (str.length() - str.replace(target, "").length()) / target.length();
    }

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        String userLine = scanner.nextLine();
        userLine.toLowerCase();
        String search = "abc abc abc";
        System.out.println(count(search, userLine));
    }
}
