package lesson8;
import java.io.IOException;
import java.util.Scanner;
public class lesson8 {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        String user = scanner.nextLine();

        System.out.println("Hello " + user + " !");
    }
}
