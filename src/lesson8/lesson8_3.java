package lesson8;
import java.io.IOException;
import java.util.Scanner;
public class lesson8_3 {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        int userLine = scanner.nextInt();
        String line = Integer.toString(userLine);
        int odd = line.length();
        if( odd / 2 != 0)
            System.out.println("true");
    }
}
