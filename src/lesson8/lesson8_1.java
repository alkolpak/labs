package lesson8;
import java.io.IOException;
import java.util.Scanner;
public class lesson8_1 {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        String userLine = scanner.nextLine();
        boolean ood = userLine.endsWith("ood");
        System.out.println(ood);
    }
}
