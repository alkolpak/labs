package lesson8;
import java.io.IOException;
import java.util.Scanner;
public class lesson8_2 {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        String userLine = scanner.nextLine();
        long java8 = userLine.codePoints().filter(ch -> ch == 'a').count();
        System.out.println("a = " + java8);
    }
}
