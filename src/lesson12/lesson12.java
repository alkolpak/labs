package lesson12;

public class lesson12 {
    public static class Address
    {
        private String country;
        private String city;
        private String street;
        private int house;
        private int index;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public int getHouse() {
            return house;
        }

        public void setHouse(int house) {
            this.house = house;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public void Out()
        {
            if((!country.isEmpty()) || (!city.isEmpty()) || (!street.isEmpty()) || house != 0 || index != 0)
            System.out.println(country + " " + city + " " + street + " " + house + " " + index);

        }
    }
    public static class Person extends Address
    {
        private String name;
        private int age;

        private String homeCountry;
        private String homeCity;
        private String homeStreet;
        private int homeHouse;
        private int homeIndex;

        private String workCountry;
        private String workCity;
        private String workStreet;
        private int workHouse;
        private int workIndex;

        public Person(String name, int age, String country, String city, String street, int house, int index,
                      String workCountry, String workCity, String workStreet, int workHouse, int workIndex)
        {
            this.name = name;
            this.age = age;
            this.homeCountry = country;
            this.homeCity = city;
            this.homeStreet = street;
            this.homeHouse = house;
            this.homeIndex = index;
            this.workCountry = workCountry;
            this.workCity = workCity;
            this.workStreet = workStreet;
            this.workHouse = workHouse;
            this.workIndex = workIndex;
        }
        public Person(String name, int age)
        {
            this.name = name;
            this.age = age;
        }
        public Person(String name, int age, String homeCountry, String homeCity, String homeStreet, int homeHouse, int homeIndex,
                      String workCountry)
        {
            this.name = name;
            this.age = age;
            this.homeCountry = homeCountry;
            this.homeCity = homeCity;
            this.homeStreet = homeStreet;
            this.homeHouse = homeHouse;
            this.homeIndex = homeIndex;
            this.workCountry = workCountry;
        }
        private String getName()
        {
            return name;
        }

        public int getAge()
        {
            return age;
        }

        public void setHomeCountry(String country) {
            this.homeCountry = country;
        }

        public void setHomeCity(String city) {
            this.homeCity = city;
        }

        public void setHomeStreet(String street) {
            this.homeStreet = street;
        }

        public void setHomeHouse(int house) {
            this.homeHouse = house;
        }

        public void setHomeIndex(int index) {
            this.homeIndex = index;
        }

        public void setWorkCountry(String country) {
            this.workCity = country;
        }

        public void setWorkCity(String city) {
            this.workCity = city;
        }

        public void setWorkStreet(String street) {
            this.workStreet = street;
        }

        public void setWorkHouse(int house) {
            this.workHouse = house;
        }

        public void setWorkIndex(int index) {
            this.workIndex = index;
        }
        private void homeAddress()
        {
            if((!homeCountry.isEmpty()) || (!homeCity.isEmpty()) || (!homeStreet.isEmpty()) || homeHouse != 0 || homeIndex != 0)
                System.out.println(homeCountry + " " + homeCity + " " + homeStreet + " " + homeHouse + " " + homeIndex);
        }
        public void workAddress()
        {
            if((!workCountry.isEmpty()) && (!workCity.isEmpty()) && (!workStreet.isEmpty()) && workHouse != 0 && workIndex != 0)
                System.out.println(workCountry + " " + workCity + " " + workStreet + " " + workHouse + " " + workIndex);
        }
    }
    public static void main(String[] args) throws IllegalArgumentException{
	Person person1 = new Person("Петя", 29, "Russia", "Moscow", "Klimashkina", 24,
            124243, "Russia", "Moscow", "Rodionova", 24, 128243);
	Person person2 = new Person("Ваня", 30, "Russia", "Moscow", "Klimashkina", 26,
            124243, "Russia", "Moscow", "Rodionova", 24, 128243);
	Person person3 = new Person("Коля", 31, "Russia", "Moscow", "Klimashkina", 16,
            124243, "Russia");
	person1.workAddress();
	person2.workAddress();
	person3.workAddress();
    }
}
